# Anasiw-aqvayli-i-Linux
Kabyle Keyboard for Linux

**Ce Clavier est basé sur le clavier crée par Adel Ferdjoukh.

Déscription:
Ce projet concerne le developpement de composants pour l'utilisation d'un clavier kabyle latin sous linux.


Vos remarques sont les bienvenues:
slimane.amiri@gmail.com


Contenu:
Les fichiers necessaires pour la configuration du clavier

Installation:
1 - Télécharger l'archive (ZIP) qui se trouve dans https://github.com/S-Amiri/Anasiw-aqvayli-i-Linux
SAUVEGARDER LES REPERTOIRES /symbols et /rules qui sont dans /usr/share/X11/xkb
2 - Extraire les fichiers dans un répértoire sur votre poste
3 - Ajouter le fichier "" kabyle "" qui est dans /symbols dans /usr/share/X11/xkb/symbols
Il faut le faire avec droit root: 

4 - Aller dans: paramètres systéme --> Agencement du clavier --> Onglet Agencements
5 - Cliquer sur le (+) en bas à gauche pour ajouter une nouvelle langue, choisir Kabyle amazigh latin et cliquer sur ajouter.

Utilisation:


alt gr + v = ɣ 	                alt gr + c = č
alt gr + w = ε                  alt gr + h = ḥ
alt gr + g = ǧ                  alt gr + s = ṣ
alt gr + d = ḍ	                alt gr + ẓ = ẓ                                                                       	             alt gr + r = ṛ                  alt gr + t = ṭ
alt gr + y = γ                  alt gr + ç = ţ   
alt gr + à= Ţ                   alt gr + è = Ẕ         
alt gr + é = ẕ                  alt gr + V = Γ 
alt gr + C = Č                  alt gr + H = Ḥ     
alt gr + G = Ǧ                  alt gr + D = Ḍ 
alt gr + S = Ṣ                  alt gr + Z = Ẓ    
alt gr + R = Ṛ                  alt gr + T = Ṭ 
alt gr + W = Σ                  alt gr + Y = Γ    
ù = Ɣ                           $ = Ɛ 
